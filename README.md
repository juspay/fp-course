# Function Programming Course created in purescript

*** To Give a try, follow below steps ***

1. Clone the Repository, followed by `bower i`, `npm i`
2. Open the corresponding file which you want to try and start implementing the functions `eg: src/Array.purs for array implementation`
3. open the corresponding test file and start adding new testsuites if you are not satisfied with default tests provided
4. Add your test function in `test/Main.purs` 
5. run `pulp test`, that's it you will get Either Error Summary
