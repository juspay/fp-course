"use strict";
var Data_Eq = require("../Data.Eq/index.js");
var Data_HeytingAlgebra = require("../Data.HeytingAlgebra/index.js");
var Prelude = require("../Prelude/index.js");
var Unsafe_Coerce = require("../Unsafe.Coerce/index.js");
var Just = (function () {
    function Just(value0) {
        this.value0 = value0;
    };
    Just.create = function (value0) {
        return new Just(value0);
    };
    return Just;
})();
var Nothing = (function () {
    function Nothing() {

    };
    Nothing.value = new Nothing();
    return Nothing;
})();
var Nil = (function () {
    function Nil() {

    };
    Nil.value = new Nil();
    return Nil;
})();
var Cons = (function () {
    function Cons(value0, value1) {
        this.value0 = value0;
        this.value1 = value1;
    };
    Cons.create = function (value0) {
        return function (value1) {
            return new Cons(value0, value1);
        };
    };
    return Cons;
})();

// merge two array by applying a function on them
// zipWith (+1) (Cons 1 (Cons 2 Nil)) (Cons 2 (Cons 3 Nil)) => (Cons 3 (Cons 5 Nil))
var zipWith = Unsafe_Coerce.unsafeCoerce;

//-- Sum of the elements in array
// sumAll (Cons 1 (Cons 2 Nil)) = 3
var sumAll = Unsafe_Coerce.unsafeCoerce;

// convert a array of Maybe values to Maybe of Array
var seqMaybe = Unsafe_Coerce.unsafeCoerce;

// reverse the array
var reverse = Unsafe_Coerce.unsafeCoerce;

// apply given function on everyelement of array
// map (+1) (Cons 1 (Cons 2 (Cons 3 Nil))) = (Cons 2 (Cons 3 (Cons 4 Nil)))
var map = Unsafe_Coerce.unsafeCoerce;

// Return length of array
// lengthFp (Cons 1 (Cons 2 Nil)) = 2
var lengthFp = Unsafe_Coerce.unsafeCoerce;

//-Exercises
// Return the head of list or default
// getHeadOr 1 Nil = 1
// getHeadOr 1 (Cons 2 (Cons 3 Nil)) = 2
var getHeadOr = Unsafe_Coerce.unsafeCoerce;

// flatten the array [[1,2],[3,4]] => [1,2,3,4]
var flatten = Unsafe_Coerce.unsafeCoerce;

// find element present in array and return index 
var find = Unsafe_Coerce.unsafeCoerce;

// apply filter on every element of array
/**
 * - filter (>2) (Cons 1 (Cons 0 (Cons 2 (Cons 3 Nil)))) = (Cons 2 (Cons 3 Nil)) -
 */
var filter = Unsafe_Coerce.unsafeCoerce;
var eqMaybe = function (dictEq) {
    return new Data_Eq.Eq(function (v) {
        return function (v1) {
            if (v instanceof Nothing && v1 instanceof Nothing) {
                return true;
            };
            if (v instanceof Just && v1 instanceof Just) {
                return Data_Eq.eq(dictEq)(v.value0)(v1.value0);
            };
            return false;
        };
    });
};
var eqCArrayInt = function (dictEq) {
    return new Data_Eq.Eq(function (v) {
        return function (v1) {
            if (v instanceof Nil && v1 instanceof Nil) {
                return true;
            };
            if (v instanceof Cons && v1 instanceof Cons) {
                return Data_Eq.eq(dictEq)(v.value0)(v1.value0) && Data_Eq.eq(eqCArrayInt(dictEq))(v.value1)(v1.value1);
            };
            return false;
        };
    });
};
module.exports = {
    Nil: Nil,
    Cons: Cons,
    Just: Just,
    Nothing: Nothing,
    getHeadOr: getHeadOr,
    sumAll: sumAll,
    lengthFp: lengthFp,
    map: map,
    filter: filter,
    flatten: flatten,
    find: find,
    reverse: reverse,
    seqMaybe: seqMaybe,
    zipWith: zipWith,
    eqCArrayInt: eqCArrayInt,
    eqMaybe: eqMaybe
};
