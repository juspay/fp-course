"use strict";
var Course_Applicative = require("../Course.Applicative/index.js");
var Course_Array = require("../Course.Array/index.js");
var Unsafe_Coerce = require("../Unsafe.Coerce/index.js");
var Bind = function (bind) {
    this.bind = bind;
};
var Monad = function (Applicative1, Bind0) {
    this.Applicative1 = Applicative1;
    this.Bind0 = Bind0;
};

// kleisli composition
var kleisli = function (dictMonad) {
    return Unsafe_Coerce.unsafeCoerce;
};
var join = function (dictMonad) {
    return Unsafe_Coerce.unsafeCoerce;
};
var bindForMaybe = new Bind(Unsafe_Coerce.unsafeCoerce);
var monadMaybe = new Monad(function () {
    return Course_Applicative.applicativeForMaybe;
}, function () {
    return bindForMaybe;
});
var bindForFunction = new Bind(Unsafe_Coerce.unsafeCoerce);
var fnMonad = new Monad(function () {
    return Course_Applicative.applicativeForFunction;
}, function () {
    return bindForFunction;
});
var bindForCArray = new Bind(Unsafe_Coerce.unsafeCoerce);
var monadForCArray = new Monad(function () {
    return Course_Applicative.applicativeForArray;
}, function () {
    return bindForCArray;
});
var bind = function (dict) {
    return dict.bind;
};
module.exports = {
    bind: bind,
    Bind: Bind,
    Monad: Monad,
    join: join,
    kleisli: kleisli,
    bindForCArray: bindForCArray,
    monadForCArray: monadForCArray,
    bindForMaybe: bindForMaybe,
    monadMaybe: monadMaybe,
    bindForFunction: bindForFunction,
    fnMonad: fnMonad
};
