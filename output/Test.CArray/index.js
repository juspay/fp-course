"use strict";
var Control_Bind = require("../Control.Bind/index.js");
var Control_Monad_Free = require("../Control.Monad.Free/index.js");
var Control_Semigroupoid = require("../Control.Semigroupoid/index.js");
var Course_Array = require("../Course.Array/index.js");
var Data_Eq = require("../Data.Eq/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Ord = require("../Data.Ord/index.js");
var Data_Ring = require("../Data.Ring/index.js");
var Data_Semiring = require("../Data.Semiring/index.js");
var Effect_Aff = require("../Effect.Aff/index.js");
var Prelude = require("../Prelude/index.js");
var Test_Unit = require("../Test.Unit/index.js");
var Test_Unit_Assert = require("../Test.Unit.Assert/index.js");
var testCArray = Test_Unit.suite("Array Test")(Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("getHeadOrNil")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be 1")(Course_Array.getHeadOr(1)(Course_Array.Nil.value) === 1))(function () {
    return Test_Unit_Assert.assert("must be 2")(Course_Array.getHeadOr(0)(new Course_Array.Cons(2, Course_Array.Nil.value)) === 2);
})))(function () {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("sumAll test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be 0")(Course_Array.sumAll(Course_Array.Nil.value) === 0))(function () {
        return Test_Unit_Assert.assert("must be 6")(Course_Array.sumAll(new Course_Array.Cons(1, new Course_Array.Cons(2, new Course_Array.Cons(3, Course_Array.Nil.value)))) === 6);
    })))(function () {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("length test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be 0")(Course_Array.lengthFp(Course_Array.Nil.value) === 0))(function () {
            return Test_Unit_Assert.assert("must be 2")(Course_Array.lengthFp(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value))) === 2);
        })))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("map test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be Nil")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.map(function (x) {
                return x + 1 | 0;
            })(Course_Array.Nil.value))(Course_Array.Nil.value)))(function () {
                return Test_Unit_Assert.assert("must be 1 added")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.map(function (x) {
                    return x + 1 | 0;
                })(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value))))(new Course_Array.Cons(2, new Course_Array.Cons(3, Course_Array.Nil.value))));
            })))(function () {
                return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("filter test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be Nil")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.filter(function (x) {
                    return x > 2;
                })(Course_Array.Nil.value))(Course_Array.Nil.value)))(function () {
                    return Test_Unit_Assert.assert("must be 1")(Course_Array.lengthFp(Course_Array.filter(function (x) {
                        return x < 1;
                    })(new Course_Array.Cons(0, new Course_Array.Cons(1, new Course_Array.Cons(3, Course_Array.Nil.value))))) === 1);
                })))(function () {
                    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("flatten test")(Test_Unit_Assert.assert("must be [1,2]")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.flatten(new Course_Array.Cons(new Course_Array.Cons(1, Course_Array.Nil.value), new Course_Array.Cons(new Course_Array.Cons(2, Course_Array.Nil.value), Course_Array.Nil.value))))(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value))))))(function () {
                        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("find Test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be Nothing")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Array.find(1)(Course_Array.Nil.value))(Course_Array.Nothing.value)))(function () {
                            return Test_Unit_Assert.assert("must be Maybe 2")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Array.find(2)(new Course_Array.Cons(1, new Course_Array.Cons(3, new Course_Array.Cons(2, Course_Array.Nil.value)))))(new Course_Array.Just(2)));
                        })))(function () {
                            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("reverse test")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("must be equal")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.reverse(new Course_Array.Cons(1, Course_Array.Nil.value)))(new Course_Array.Cons(1, Course_Array.Nil.value))))(function () {
                                return Test_Unit_Assert.assert("must be reverse")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.reverse(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value))))(new Course_Array.Cons(2, new Course_Array.Cons(1, Course_Array.Nil.value))));
                            })))(function () {
                                return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("seq test")(Test_Unit_Assert.assert("array to maybe")(Data_Eq.eq(Course_Array.eqMaybe(Course_Array.eqCArrayInt(Data_Eq.eqInt)))(Course_Array.seqMaybe(new Course_Array.Cons(new Course_Array.Just(1), new Course_Array.Cons(Course_Array.Nothing.value, Course_Array.Nil.value))))(new Course_Array.Just(new Course_Array.Cons(1, Course_Array.Nil.value))))))(function () {
                                    return Test_Unit.test("zip test")(Test_Unit_Assert.assert("add two arrays")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Array.zipWith(function (x) {
                                        return function (y) {
                                            return x + y | 0;
                                        };
                                    })(new Course_Array.Cons(1, new Course_Array.Cons(4, Course_Array.Nil.value)))(new Course_Array.Cons(-1 | 0, new Course_Array.Cons(4, Course_Array.Nil.value))))(new Course_Array.Cons(0, new Course_Array.Cons(8, Course_Array.Nil.value)))));
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}));
module.exports = {
    testCArray: testCArray
};
