"use strict";
var Control_Bind = require("../Control.Bind/index.js");
var Control_Monad_Free = require("../Control.Monad.Free/index.js");
var Course_Array = require("../Course.Array/index.js");
var Course_Functor = require("../Course.Functor/index.js");
var Data_Eq = require("../Data.Eq/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Semiring = require("../Data.Semiring/index.js");
var Data_Unit = require("../Data.Unit/index.js");
var Effect_Aff = require("../Effect.Aff/index.js");
var Prelude = require("../Prelude/index.js");
var Test_Unit = require("../Test.Unit/index.js");
var Test_Unit_Assert = require("../Test.Unit.Assert/index.js");
var functorTest = Test_Unit.suite("functor testing")(Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("map for Array")(Test_Unit_Assert.assert("map basic array")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Functor.map(Course_Functor.mapForCArray)(function (val) {
    return val + 1 | 0;
})(new Course_Array.Cons(1, new Course_Array.Cons(3, Course_Array.Nil.value))))(new Course_Array.Cons(2, new Course_Array.Cons(4, Course_Array.Nil.value))))))(function () {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("map for maybe")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("map maybe ")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Functor.map(Course_Functor.mapForMaybe)(function (val) {
        return val + 2 | 0;
    })(new Course_Array.Just(2)))(new Course_Array.Just(4))))(function () {
        return Test_Unit_Assert.assert("map Nothing")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Functor.map(Course_Functor.mapForMaybe)(function (val) {
            return val + 2 | 0;
        })(Course_Array.Nothing.value))(Course_Array.Nothing.value));
    })))(function () {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("map for function")(Test_Unit_Assert.assert("map function")(Course_Functor.map(Course_Functor.mapForFunction)(function (val) {
            return val + 1 | 0;
        })(function (val) {
            return val * 2 | 0;
        })(8) === 17)))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("map for void Right")(Test_Unit_Assert.assert("for void Right")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Functor.voidRight(Course_Functor.mapForCArray)(2)(new Course_Array.Cons("me", Course_Array.Nil.value)))(new Course_Array.Cons(2, Course_Array.Nil.value)))))(function () {
                return Test_Unit.test("map for void")(Test_Unit_Assert.assert("for void")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqUnit))(Course_Functor["void"](Course_Functor.mapForCArray)(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value))))(new Course_Array.Cons(Data_Unit.unit, new Course_Array.Cons(Data_Unit.unit, Course_Array.Nil.value)))));
            });
        });
    });
}));
module.exports = {
    functorTest: functorTest
};
