"use strict";
var Control_Applicative = require("../Control.Applicative/index.js");
var Control_Bind = require("../Control.Bind/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Functor = require("../Data.Functor/index.js");
var Data_Traversable = require("../Data.Traversable/index.js");
var Data_Unit = require("../Data.Unit/index.js");
var Effect = require("../Effect/index.js");
var Prelude = require("../Prelude/index.js");
var Test_Applicative = require("../Test.Applicative/index.js");
var Test_CArray = require("../Test.CArray/index.js");
var Test_Functor = require("../Test.Functor/index.js");
var Test_Monad = require("../Test.Monad/index.js");
var Test_Unit_Main = require("../Test.Unit.Main/index.js");
var main = function __do() {
    var v = Data_Traversable.sequence(Data_Traversable.traversableArray)(Effect.applicativeEffect)(Data_Functor.map(Data_Functor.functorArray)(Test_Unit_Main.runTest)([ Test_CArray.testCArray, Test_Functor.functorTest, Test_Applicative.applicativeTest, Test_Monad.monadTest ]))();
    return Data_Unit.unit;
};
module.exports = {
    main: main
};
