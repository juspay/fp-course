"use strict";
var Control_Bind = require("../Control.Bind/index.js");
var Control_Monad_Free = require("../Control.Monad.Free/index.js");
var Course_Array = require("../Course.Array/index.js");
var Course_Monad = require("../Course.Monad/index.js");
var Data_Eq = require("../Data.Eq/index.js");
var Data_Function = require("../Data.Function/index.js");
var Data_Semiring = require("../Data.Semiring/index.js");
var Effect_Aff = require("../Effect.Aff/index.js");
var Prelude = require("../Prelude/index.js");
var Test_Unit = require("../Test.Unit/index.js");
var Test_Unit_Assert = require("../Test.Unit.Assert/index.js");
var monadTest = Test_Unit.suite("monad test starts")(Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("bind and monad test for carray")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert(" bind for carray")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Monad.bind(Course_Monad.bindForCArray)(new Course_Array.Cons(2, Course_Array.Nil.value))(function (a) {
    return new Course_Array.Cons(a, Course_Array.Nil.value);
}))(new Course_Array.Cons(2, Course_Array.Nil.value))))(function () {
    return Test_Unit_Assert.assert("bind for carray Nil")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Monad.bind(Course_Monad.bindForCArray)(Course_Array.Nil.value)(function (a) {
        return new Course_Array.Cons(a + 1 | 0, Course_Array.Nil.value);
    }))(Course_Array.Nil.value));
})))(function () {
    return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("bind and monad test for maybe")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("bind for maybe")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Monad.bind(Course_Monad.bindForMaybe)(new Course_Array.Just(2))(function (a) {
        return new Course_Array.Just(a);
    }))(new Course_Array.Just(2))))(function () {
        return Test_Unit_Assert.assert("bind for maybe nil")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Monad.bind(Course_Monad.bindForMaybe)(Course_Array.Nothing.value)(function (a) {
            return new Course_Array.Just(a + 1 | 0);
        }))(Course_Array.Nothing.value));
    })))(function () {
        return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("bind and monad test for function")(Test_Unit_Assert.assert("bind and monad for function")(Course_Monad.bind(Course_Monad.bindForFunction)(function (a) {
            return a + 10 | 0;
        })(function (a) {
            return function (b) {
                return a * b | 0;
            };
        })(7) === 119)))(function () {
            return Control_Bind.discard(Control_Bind.discardUnit)(Control_Monad_Free.freeBind)(Test_Unit.test("test join ")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("join with CArray")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Monad.join(Course_Monad.monadForCArray)(new Course_Array.Cons(new Course_Array.Cons(1, Course_Array.Nil.value), new Course_Array.Cons(new Course_Array.Cons(2, Course_Array.Nil.value), Course_Array.Nil.value))))(new Course_Array.Cons(1, new Course_Array.Cons(2, Course_Array.Nil.value)))))(function () {
                return Test_Unit_Assert.assert("join with Maybe")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Monad.join(Course_Monad.monadMaybe)(new Course_Array.Just(new Course_Array.Just(2))))(new Course_Array.Just(2)));
            })))(function () {
                return Test_Unit.test("test kliesli")(Control_Bind.discard(Control_Bind.discardUnit)(Effect_Aff.bindAff)(Test_Unit_Assert.assert("kliesli with CArray")(Data_Eq.eq(Course_Array.eqCArrayInt(Data_Eq.eqInt))(Course_Monad.kleisli(Course_Monad.monadForCArray)(function (a) {
                    return new Course_Array.Cons(a, Course_Array.Nil.value);
                })(function (b) {
                    return new Course_Array.Cons(b, Course_Array.Nil.value);
                })(2))(new Course_Array.Cons(2, Course_Array.Nil.value))))(function () {
                    return Test_Unit_Assert.assert("kliesli with Maybe")(Data_Eq.eq(Course_Array.eqMaybe(Data_Eq.eqInt))(Course_Monad.kleisli(Course_Monad.monadMaybe)(function (a) {
                        return new Course_Array.Just(a * 3 | 0);
                    })(function (b) {
                        return new Course_Array.Just(b + 1 | 0);
                    })(3))(new Course_Array.Just(12)));
                }));
            });
        });
    });
}));
module.exports = {
    monadTest: monadTest
};
